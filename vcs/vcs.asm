.macro init() {
	sei
	cld
	ldx #$ff
	txs
	lda #0
	clear_zeropage:
		sta $00,x
		dex
		bne clear_zeropage		
}

.macro  timwait() {
	wait:
		lda INTIM
		bne wait
				
}

.macro wsync() {
	sta WSYNC	
}

.macro sync1() {
	:timwait()
	:wsync()
	lda #2
	sta VBLANK
	lda #40
	sta TIM64T		
}

.macro sync2() {
	:timwait()
	:wsync()
	lda #2
	sta VSYNC
	:wsync()
	:wsync()
	lda #0
	:wsync()
	sta VSYNC
	lda #54
	sta TIM64T		
}


.macro sync3() {
	:timwait()
	:wsync()
	lda #0
	sta VBLANK
	lda #18
	sta T1024T		
}

.label VSYNC	= $00		// $00   0000 00x0   Vertical Sync Set-Clear
.label VBLANK	= $01		// $01   xx00 00x0   Vertical Blank Set-Clear
.label WSYNC	= $02		// $02   ---- ----   Wait for Horizontal Blank
.label RSYNC	= $03		// $03   ---- ----   Reset Horizontal Sync Counter
.label NUSIZ0	= $04		// $04   00xx 0xxx   Number-Size player/missle 0
.label NUSIZ1	= $05		// $05   00xx 0xxx   Number-Size player/missle 1
							//			   ^^^	- Player-Missile number & Player size (See table bel
							//			   000		  0  One copy              (X.........)
							//			   001		  1  Two copies - close    (X.X.......)
							//			   010		  2  Two copies - medium   (X...X.....)
							//			   011		  3  Three copies - close  (X.X.X.....)
							//			   100		  4  Two copies - wide     (X.......X.)
							//			   101		  5  Double sized player   (XX........)
							//			   110		  6  Three copies - medium (X...X...X.)
							//			   111		  7  Quad sized player     (XXXX......)
							//		   ^^			- Missile Size  (0..3 = 1,2,4,8 pixels width)


.label COLUP0	= $06		// $06   xxxx xxx0   Color-Luminance Player 0
.label COLUP1	= $07		// $07   xxxx xxx0   Color-Luminance Player 1
.label COLUPF	= $08		// $08   xxxx xxx0   Color-Luminance Playfield
.label COLUBK	= $09		// $09   xxxx xxx0   Color-Luminance Background
.label CTRLPF	= $0A		// $0A   00xx 0xxx   Control Playfield, Ball, Collisions
							//				 ^	- Playfield Reflection     (0=Normal, 1=Mirror right half)
							//				^		- Playfield Color          (0=Normal, 1=Score Mode, only if Bit2=0)
							//			   ^		- Playfield/Ball Priority  (0=Normal, 1=Above Players/Missiles)
							//		   ^^			- Ball size                (0..3 = 1,2,4,8 pixels width)


.label REFP0	= $0B		// $0B   0000 x000   Reflection Player 0
.label REFP1	= $0C		// $0C   0000 x000   Reflection Player 1
.label PF0		= $0D		// $0D   xxxx 0000   Playfield Register Byte 0
.label PF1		= $0E		// $0E   xxxx xxxx   Playfield Register Byte 1
.label PF2		= $0F		// $0F   xxxx xxxx   Playfield Register Byte 2
.label RESP0	= $10		// $10   ---- ----   Reset Player 0
.label RESP1	= $11		// $11   ---- ----   Reset Player 1
.label RESM0	= $12		// $12   ---- ----   Reset Missle 0
.label RESM1	= $13		// $13   ---- ----   Reset Missle 1
.label RESBL	= $14		// $14   ---- ----   Reset Ball
.label AUDC0	= $15		// $15   0000 xxxx   Audio Control 0
.label AUDC1	= $16		// $16   0000 xxxx   Audio Control 1
.label AUDF0	= $17		// $17   000x xxxx   Audio Frequency 0
.label AUDF1	= $18		// $18   000x xxxx   Audio Frequency 1
.label AUDV0	= $19		// $19   0000 xxxx   Audio Volume 0
.label AUDV1	= $1A		// $1A   0000 xxxx   Audio Volume 1
.label GRP0		= $1B		// $1B   xxxx xxxx   Graphics Register Player 0
.label GRP1		= $1C		// $1C   xxxx xxxx   Graphics Register Player 1
.label ENAM0	= $1D		// $1D   0000 00x0   Graphics Enable Missle 0
.label ENAM1	= $1E		// $1E   0000 00x0   Graphics Enable Missle 1
.label ENABL	= $1F		// $1F   0000 00x0   Graphics Enable Ball
.label HMP0		= $20		// $20   xxxx 0000   Horizontal Motion Player 0
.label HMP1		= $21		// $21   xxxx 0000   Horizontal Motion Player 1
.label HMM0		= $22		// $22   xxxx 0000   Horizontal Motion Missle 0
.label HMM1		= $23		// $23   xxxx 0000   Horizontal Motion Missle 1
.label HMBL		= $24		// $24   xxxx 0000   Horizontal Motion Ball
.label VDELP0	= $25		// $25   0000 000x   Vertical Delay Player 0
.label VDELP1	= $26		// $26   0000 000x   Vertical Delay Player 1
.label VDELBL	= $27		// $27   0000 000x   Vertical Delay Ball
.label RESMP0	= $28		// $28   0000 00x0   Reset Missle 0 to Player 0
.label RESMP1	= $29		// $29   0000 00x0   Reset Missle 1 to Player 1
.label HMOVE	= $2A		// $2A   ---- ----   Apply Horizontal Motion
.label HMCLR	= $2B		// $2B   ---- ----   Clear Horizontal Move Registers
.label CXCLR	= $2C		// $2C   ---- ----   Clear Collision Latches

.label CXM0P	= $00	    // $00  xx00 0000       Read Collision  M0-P1   M0-P0
.label CXM1P	= $01		// $01  xx00 0000                       M1-P0   M1-P1
.label CXP0FB	= $02		// $02  xx00 0000                       P0-PF   P0-BL
.label CXP1FB	= $03		// $03  xx00 0000                       P1-PF   P1-BL
.label CXM0FB	= $04		// $04  xx00 0000                       M0-PF   M0-BL
.label CXM1FB	= $05		// $05  xx00 0000                       M1-PF   M1-BL
.label CXBLPF	= $06		// $06  x000 0000                       BL-PF   -----
.label CXPPMM	= $07		// $07  xx00 0000                       P0-P1   M0-M1
.label INPT0	= $08		// $08  x000 0000       Read Pot Port 0
.label INPT1	= $09		// $09  x000 0000       Read Pot Port 1
.label INPT2	= $0A		// $0A  x000 0000       Read Pot Port 2
.label INPT3	= $0B		// $0B  x000 0000       Read Pot Port 3
.label INPT4	= $0C		// $0C	x000 0000       Read Input (Trigger) 0
.label INPT5	= $0D		// $0D	x000 0000       Read Input (Trigger) 1

.label SWCHA	= $280		// $280     Port A data register for joysticks:
							//			Bits 4-7 for player 1.  Bits 0-3 for player 2.

.label SWACNT	= $281		// $281     Port A data direction register (DDR)
.label SWCHB	= $282		// $282		Port B data (console switches)
.label SWBCNT	= $283		// $283     Port B DDR
.label INTIM	= $284		// $284		Timer output
.label TIMINT   = $285  	// $285

.label TIM1T	= $294		// $294		set 1 clock interval
.label TIM8T	= $295		// $295     set 8 clock interval
.label TIM64T	= $296		// $296     set 64 clock interval
.label T1024T	= $297		// $297     set 1024 clock interval
