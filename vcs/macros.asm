// koszt 15 cykli
.macro moveline(line, HM, nachylenie) {
	lda line //3 cykle
	clc //2 cykle
	adc #nachylenie //2 cykle
	sta HM //3 cykle
	and #%00001111 //2 cykle
	sta line //3 cykle
}


.macro logo(logo, HM, nachylenie) {
	lda logo //3 cykle
	clc //2 cykle
	adc #nachylenie //2 cykle
	sta HM //3 cykle
	and #%00001111 //2 cykle
	sta logo //3 cykle
}
.macro logokk(logo, HM) {
	clc //2 cykle
	adc logo
	sta HM //3 cykle
	and #%00001111 //2 cykle
	sta logo //3 cykle
}
.macro NOPY (ILE) {
	.for(var i=0; i<ILE; i++) {
		nop	
	}
}