.import source "vcs.asm"
.import source "macros.asm"

//labele
 .label line1 = $80
 .label line2 = $80+1
 .label line3 = $80+2
 .label line4 = $80+3
 .label line5 = $80+4

 .label logo1 = $80+5
 .label logo2 = $80+6
 .label logo3 = $80+7
 .label anim  = $80+8
 .label songpos_seq = $80 +9
 .label songpos_step = $80 +10
 .label songpos_tick = $80 +11
 .label ptra = $80 +12 //wskazniki biora 2 bajty
 .label ptrb = $80 +14 //wskazniki biora 2 bajty
 .label anim2  = $80+16
 .label animtemp = $80+17

.label ac0   = $15       // $15   0000 xxxx   Audio Control 0
.label ac1    = $16       // $16   0000 xxxx   Audio Control 1
.label af0    = $17       // $17   000x xxxx   Audio Frequency 0
.label af1    = $18       // $18   000x xxxx   Audio Frequency 1
.label av0    = $19       // $19   0000 xxxx   Audio Volume 0
.label av1    = $1A       // $1A   0000 xxxx   Audio Volume 1

.pc = $1000 "Start"
.import source "music.asm"
.import source "tables.asm"


LineTable2:
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %11111111   
    .byte 0
    .byte 0
    .byte 0    
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %11111111   
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %11111111  
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %11111111
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0

LineTable:
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %11111111   
    .byte 0
    .byte 0
    .byte 0    
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %00111111   
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %00011111  
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte %00000111
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0
    .byte 0

LineTableCol:
	.byte $70
	.byte $70
	.byte $70
	.byte $70
	.byte $70
	.byte $70
	.byte $70
	.byte $70
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $72
	.byte $74
	.byte $74
	.byte $74
	.byte $74
	.byte $74
	.byte $74
	.byte $74
	.byte $74
	.byte $76
	.byte $76
	.byte $76
	.byte $76
	.byte $76
	.byte $76
	.byte $76
	.byte $78
	.byte $78
	.byte $78
	.byte $7A
	.byte $7C
	.byte $7E


S:
    .byte %01111110
    .byte %11111111
    .byte %11000000
    .byte %11111110
    .byte %01111111
    .byte %00000011
    .byte %11111111
    .byte %01111110

V:
    .byte %11000011
    .byte %11000011
    .byte %11000011
    .byte %01100110
    .byte %01100110
    .byte %01100110
    .byte %00111100
    .byte %00011000

II:
    .byte %01111110 
    .byte %11111111
    .byte %00000011
    .byte %00001110
    .byte %00111100
    .byte %01110000
    .byte %11111111
    .byte %11111111

ZERO:
    .byte %01111110
    .byte %11111111
    .byte %11000011
    .byte %11000011
    .byte %11000011
    .byte %11000011
    .byte %11111111
    .byte %01111110

I:
    .byte %00011000
    .byte %00111000
    .byte %01111000
    .byte %00011000
    .byte %00011000
    .byte %00011000
    .byte %00111100
    .byte %00111100

VII:
    .byte %11111111
    .byte %01111111
    .byte %00000011
    .byte %00000110
    .byte %00001100
    .byte %00011000
    .byte %00011000
    .byte %00011000

SillyCols:
    .byte $3E
    .byte $3C
    .byte $3A
    .byte $38
    .byte $36
    .byte $34
    .byte $32
    .byte $30

LogoColors:
    .byte $8E
    .byte $8C
    .byte $8A
    .byte $88
    .byte $86
    .byte $84
    .byte $82
    .byte $80
    .byte $8E
    .byte $8C
    .byte $8A
    .byte $88
    .byte $86
    .byte $84
    .byte $82
    .byte $80
    .byte $8E
    .byte $8C
    .byte $8A
    .byte $88
    .byte $86
    .byte $84
    .byte $82
    .byte $80
    .byte $8E
    .byte $8C
    .byte $8A
    .byte $88
    .byte $86
    .byte $84
    .byte $82
    .byte $80
    .byte $8E
    .byte $8C
    .byte $8A
    .byte $88
    .byte $86
    .byte $84
    .byte $82
    .byte $80

start:
    :init()
//główna pętla
mainloop:
    :sync1()
    // tu mam 30 skalineów na obliczenia
    jsr song_player
    :sync2()
    // tu mam 37 skanlinów na obliczenia
    :sync3()
    // rysowanie ekranu
    //inx
    //stx COLUBK
    :wsync()
    :wsync()
    :wsync()
    lda #0
    sta COLUBK
    
    :wsync()
    .for(var i=0; i<21; i++) {
        nop
    }
    sta RESP0
    sta RESBL
    sta RESP1
     
    lda #1
    sta CTRLPF
     
//Atari Logo

   .for(var i=0; i<10; i++) {
        :wsync()
    }

    inc anim
 
    :wsync()
    lda #0
    sta PF0
    sta PF1
    sta PF2

     lda anim
     lsr
     lsr
     and 7
     tax

    //lda #$E0   // 8..E   8+x
    lda Logo0a,x
    sta HMP0
//    lda #$10    // 7..1     7-x  x=0..6
    lda Logo0b,x
    sta HMP1
    lda #0
    sta HMBL

    :wsync()
    sta HMOVE
    :wsync()
    lda #$00
    sta HMP0
    lda #$30 
    sta HMP1
    :wsync()
    sta HMOVE

    lda #$8a
    sta COLUP0
    sta COLUP1
    sta COLUPF
    lda #%11110000
    sta GRP0
    sta GRP1
    lda #%00000010
    sta ENABL
    lda #%00100000
    sta CTRLPF

    lda #%00001111
    sta HMP0
    sta HMP1

    lda #8
    sta logo1
    sta logo3
    sta logo2
    
     lda anim
     lsr
     lsr
     and 7
     sta animtemp
	 tax
    
    ldy #15
    drawlogo0:		// y = 15..1
         :wsync()
		// gradient
		tya
		clc
		adc animtemp
		tax
		lda LogoColors+3,x
		sta COLUP0
		sta COLUP1
		sta COLUPF
         :wsync()

        //dekrementacja
        dey
        bne drawlogo0


    ldy #10
    drawlogo:		// y = 10..1
        :wsync()
		// gradient
		tya
		clc
		adc animtemp
		tax
		lda LogoColors+9,x
		sta COLUP0
		sta COLUP1
		sta COLUPF

        //dochylanie atari logo
		ldx animtemp
        lda Logo1,x
        :logokk(logo1, HMP0)
        sec
        lda #0
        sbc Logo1,x
        :logokk(logo3, HMP1)   
        // koniec odchylania logo atari

        :wsync()
        sta HMOVE

        //dekrementacja
        dey
        bne drawlogo


    ldy #5
    drawlogo2:			// y = 5..1
        :wsync()
		// gradient
		tya
		clc
		adc animtemp
		tax
		lda LogoColors+4,x
		sta COLUP0
		sta COLUP1
		sta COLUPF

        //dochylanie atari logo
		ldx animtemp
        lda Logo2,x
        :logokk(logo1, HMP0)
        sec
        lda #0
        sbc Logo2,x
        :logokk(logo3, HMP1)   
        // koniec odchylania logo atari
        :wsync()
        sta HMOVE + 256
        //dekrementacja
        dey
        bne drawlogo2

    ldy #5
    drawlogo3:			// y = 5..1
        :wsync()
		// gradient
		tya
		clc
		adc animtemp
		tax
		lda LogoColors-1,x
		sta COLUP0
		sta COLUP1
		sta COLUPF

        //dochylanie atari logo
		ldx animtemp
        lda Logo3,x
        :logokk(logo1, HMP0)
        sec
        lda #0
        sbc Logo3,x
        :logokk(logo3, HMP1)   
        // koniec odchylania logo atari
        // koniec odchylania logo atari

        :wsync()
        sta HMOVE + 256
        //dekrementacja
        dey
        bne drawlogo3


    lda #0
    sta GRP0
    sta GRP1
    sta ENAM0
    sta ENAM1
    sta ENABL

//End of Atari Logo

.for(var i=0; i<16; i++) {
    :wsync()
}

//Napisy

lda #%00000110
sta NUSIZ0
sta NUSIZ1
:wsync()
.for(var i=0; i<16; i++) {
    nop
}

sta RESP0
nop
sta RESP1



.for(var i=0; i<16; i++) {
    :wsync()

    lda SillyCols+i/2		// 4 cykle
    sta COLUP0				// 3
    sta COLUP1				// 3

    lda S + i/2
    sta GRP0
    
    lda V +i/2
    sta GRP1
    
    :NOPY(5)
    lda II +i/2
    sta GRP0
    
    lda ZERO +i/2
    sta GRP1

    lda I +i/2
    sta GRP0

    lda VII +i/2
    sta GRP1
}


lda #%00000000
sta NUSIZ0

sta NUSIZ1


lda #%00000000
sta GRP0
sta GRP1

//End of napisy




//Kratownica
 
    :wsync()
    .for(var i=0; i<19; i++) {
        nop
    }
    sta RESP0
    sta RESP1
    nop
    sta RESM0
    sta RESM1
    sta RESBL
     
    lda #1
    sta CTRLPF

   .for(var i=0; i<20; i++) {
        :wsync()
    }
 
    :wsync()
    lda #0
    sta PF0
    sta PF1
    sta PF2

    lda #$30
    sta HMM0
    lda #$00 
    sta HMM1
    lda #$20
    sta HMP1
    lda #$70
    sta HMP0
    lda #256 -$50
    sta HMBL

    :wsync()
    sta HMOVE

    lda #0
    sta line1
    sta line2
    sta line3
    sta line4
    sta line5
    ldx #40

	// Inkrementacja anim2	0..(11*4-1)
	inc anim2
	lda anim2
	cmp	#11*4
	bne noanim2reset
	lda #0
	sta anim2
noanim2reset:
	// Acc = anim2
	lsr		// dzielenie przez 4
	lsr
	clc
	adc #40
    tay			// Y = anim2/4 + 40

    lda #$7c
    sta COLUP0
    sta COLUP1
    sta COLUPF
    lda #%10000000
    sta GRP0
    sta GRP1
    lda #2
    sta ENAM0
    sta ENAM1
    sta ENABL
    //lda #$10
    lda #0
    sta HMP0
    sta HMP1


    lda LineTable, y
    sta PF1
    lda LineTable2, y
    sta PF2
	dey
	
    drawline:
        :wsync()
        :moveline(line1, HMP0, 12)
        :moveline(line2, HMP1, 6)
        :moveline(line3, HMM0, 0)
        :moveline(line4, HMM1, -6)
        :moveline(line5, HMBL, -12)

        //:wsync()
        sta HMOVE + 256
        
        lda LineTable, y
        sta PF1
        lda LineTable2, y
        sta PF2

        lda LineTableCol, x
	    sta COLUP0
	    sta COLUP1
		sta COLUPF
        
        //dekrementacja
		dey
        dex
        bne drawline

	:wsync()
 
    lda #0
    sta GRP0
    sta GRP1
    sta ENAM0
    sta ENAM1
    sta ENABL
	sta PF1
	sta PF2
//End of Kratownica

    jmp mainloop
.pc = $2000 - 4 "End"
.word start,start
