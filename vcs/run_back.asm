.import source "vcs.asm"
.import source "macros.asm"

//labele
 .label line1 = $80
 .label line2 = $80+1
 .label line3 = $80+2
 .label line4 = $80+3
 .label line5 = $80+4

.pc = $1000 "Start"

start:
    :init()
//główna pętla
mainloop:
    :sync1()
    // tu mam 30 skalineów na obliczenia
    :sync2()
    // tu mam 37 skanlinów na obliczenia
    :sync3()
    // rysowanie ekranu
    inx
    stx COLUBK
    :wsync()
    :wsync()
    :wsync()
    lda #0
    sta COLUBK

    :wsync()
    .for(var i=0; i<19; i++) {
        nop
    }
    sta RESP0
    sta RESP1
    nop
    sta RESM0
    sta RESM1
    sta RESBL
     
    lda #1
    sta CTRLPF
   
    :wsync()
    lda #%10101010
    sta PF0
    sta PF1
    sta PF2

    :wsync()
    lda #0
    sta PF0
    sta PF1
    sta PF2

    lda #$30
    sta HMM0
    lda #$00 
    sta HMM1
    lda #$20
    sta HMP1
    lda #$70
    sta HMP0
    lda #256- $50
    sta HMBL

    :wsync()
    sta HMOVE

    lda #$7c
    sta COLUP0
    sta COLUP1
    sta COLUPF
    lda #%10000000
    sta GRP0
    sta GRP1
    lda #2
    sta ENAM0
    sta ENAM1
    sta ENABL
    //lda #$10
    lda #0
    sta HMP0
    sta HMP1


    lda #0
    sta line1
    sta line2
    sta line3
    sta line4
    sta line5
    ldx #40
    drawline:
        :wsync()
        :moveline(line1, HMP0, 12)
        :moveline(line2, HMP1, 6)
        :moveline(line3, HMM0, 0)
        :moveline(line4, HMM1, -6)
        :moveline(line5, HMBL, -12)
        
        //:wsync()
        sta HMOVE + 256
        dex
        bne drawline
    
    lda #0
    sta GRP0
    sta GRP1
    sta ENAM0
    sta ENAM1
    sta ENABL
    jmp mainloop



.pc = $2000 - 4 "End"
.word start,start
