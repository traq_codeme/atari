

//  // seq  ch0  ch1  ch2  ch3
// section songpata
.pc = * "align"
 .align 256
.pc = * "songpata"
songpata:
//  	/* pattern  0 */	0x00 0x05 0x05 0x05 0x0d 0x05 0x05 0x05 0x00 0x05 0x05 0x05 0x0d 0x05 0x05 0x05 
    .byte $00
    .byte $05
    .byte $05
    .byte $05
    .byte $0d
    .byte $05
    .byte $05
    .byte $05
    .byte $00
    .byte $05
    .byte $05
    .byte $05
    .byte $0d
    .byte $05
    .byte $05
    .byte $05
//  	/* pattern  1 */	0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
//  	/* pattern  2 */	0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
//  	/* pattern  3 */	0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
//  	/* pattern  4 */	0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
//  	/* pattern  5 */	0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
//  	/* pattern  6 */	0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 0x1d 0x24 
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
    .byte $1d
    .byte $24
//  	/* pattern  7 */	0x15 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 
    .byte $15
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
//  	/* pattern  8 */	0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
//  	/* pattern  9 */	0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x2c 0x15 0x2c 0x15 0x2c 0x15 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $2c
    .byte $15
    .byte $2c
    .byte $15
    .byte $2c
    .byte $15
//  	/* pattern 10 */	0x2c 0x30 0x15 0x05 0x2c 0x30 0x15 0x05 0x2c 0x15 0x2c 0x30 0x2f 0x15 0x2c 0x30 
    .byte $2c
    .byte $30
    .byte $15
    .byte $05
    .byte $2c
    .byte $30
    .byte $15
    .byte $05
    .byte $2c
    .byte $15
    .byte $2c
    .byte $30
    .byte $2f
    .byte $15
    .byte $2c
    .byte $30
//  	/* pattern 11 */	0x15 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x2c 0x30 0x15 0x05 0x2c 0x15 0x2c 0x30 
    .byte $15
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $2c
    .byte $30
    .byte $15
    .byte $05
    .byte $2c
    .byte $15
    .byte $2c
    .byte $30
//  	/* pattern 12 */	0x15 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 
    .byte $15
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
//  	/* pattern 13 */	0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x2c 0x15 0x2c 0x15 0x2c 0x15 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $2c
    .byte $15
    .byte $2c
    .byte $15
    .byte $2c
    .byte $15
//  	/* pattern 14 */	0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
//  	/* pattern 15 */	0x05 0x05 0x05 0x05 0x05 0x05 0x38 0x15 0x38 0x40 0x38 0x40 0x38 0x40 0x38 0x40 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $38
    .byte $15
    .byte $38
    .byte $40
    .byte $38
    .byte $40
    .byte $38
    .byte $40
    .byte $38
    .byte $40
//  	/* pattern 16 */	0x15 0x05 0x38 0x40 0x15 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 0x05 
    .byte $15
    .byte $05
    .byte $38
    .byte $40
    .byte $15
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
//  	/* pattern 17 */	0x05 0x05 0x05 0x05 0x05 0x05 0x38 0x15 0x38 0x15 0x38 0x40 0x15 0x05 0x38 0x40 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $38
    .byte $15
    .byte $38
    .byte $15
    .byte $38
    .byte $40
    .byte $15
    .byte $05
    .byte $38
    .byte $40
//  	/* pattern 18 */	0x05 0x05 0x05 0x05 0x05 0x05 0x38 0x15 0x38 0x15 0x38 0x40 0x38 0x40 0x38 0x40 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $38
    .byte $15
    .byte $38
    .byte $15
    .byte $38
    .byte $40
    .byte $38
    .byte $40
    .byte $38
    .byte $40
//  	/* pattern 19 */	0x15 0x05 0x38 0x40 0x15 0x05 0x38 0x40 0x15 0x05 0x05 0x05 0x05 0x05 0x05 0x05 
    .byte $15
    .byte $05
    .byte $38
    .byte $40
    .byte $15
    .byte $05
    .byte $38
    .byte $40
    .byte $15
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $05
//  	/* pattern 20 */	0x05 0x05 0x05 0x05 0x38 0x15 0x38 0x15 0x38 0x15 0x38 0x40 0x15 0x05 0x38 0x40 
    .byte $05
    .byte $05
    .byte $05
    .byte $05
    .byte $38
    .byte $15
    .byte $38
    .byte $15
    .byte $38
    .byte $15
    .byte $38
    .byte $40
    .byte $15
    .byte $05
    .byte $38
    .byte $40
//  	/* pattern 21 */	0x05 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 0x15 
    .byte $05
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
    .byte $15
//  	/* pattern 22 */	0x48 0x50 0x58 0x60 0x66 0x6d 0x74 0x7b 0x7e 0x7e 0x7e 0x7e 0x48 0x50 0x48 0x50 
    .byte $48
    .byte $50
    .byte $58
    .byte $60
    .byte $66
    .byte $6d
    .byte $74
    .byte $7b
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $48
    .byte $50
    .byte $48
    .byte $50
//  	/* pattern 23 */	0x57 0x5f 0x66 0x6d 0x82 0x7e 0x7e 0x7e 0x7d 0x7d 0x7d 0x7d 0x48 0x50 0x48 0x50 
    .byte $57
    .byte $5f
    .byte $66
    .byte $6d
    .byte $82
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $48
    .byte $50
    .byte $48
    .byte $50
//  	/* pattern 24 */	0x57 0x5f 0x66 0x6d 0x82 0x7e 0x7e 0x7e 0x7d 0x7d 0x7d 0x7d 0x48 0x50 0x48 0x50 
    .byte $57
    .byte $5f
    .byte $66
    .byte $6d
    .byte $82
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $48
    .byte $50
    .byte $48
    .byte $50
//  	/* pattern 25 */	0x57 0x5f 0x66 0x6d 0x82 0x7e 0x7e 0x7e 0x7d 0x7d 0x7d 0x7d 0x7c 0x7c 0x7c 0x7c 
    .byte $57
    .byte $5f
    .byte $66
    .byte $6d
    .byte $82
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7c
    .byte $7c
    .byte $7c
    .byte $7c
//  	/* pattern 26 */	0x48 0x50 0x58 0x60 0x66 0x6d 0x74 0x7b 0x7e 0x7e 0x7e 0x7e 0x7d 0x7d 0x48 0x50 
    .byte $48
    .byte $50
    .byte $58
    .byte $60
    .byte $66
    .byte $6d
    .byte $74
    .byte $7b
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7d
    .byte $7d
    .byte $48
    .byte $50
//  	/* pattern 27 */	0x57 0x5f 0x66 0x6d 0x82 0x7e 0x7e 0x7e 0x7d 0x7d 0x7d 0x7d 0x7c 0x7c 0x48 0x50 
    .byte $57
    .byte $5f
    .byte $66
    .byte $6d
    .byte $82
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7c
    .byte $7c
    .byte $48
    .byte $50
//  	/* pattern 28 */	0x57 0x5f 0x66 0x6d 0x82 0x7e 0x7e 0x7e 0x7d 0x7d 0x7d 0x7d 0x7c 0x7c 0x48 0x50 
    .byte $57
    .byte $5f
    .byte $66
    .byte $6d
    .byte $82
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7c
    .byte $7c
    .byte $48
    .byte $50
//  	/* pattern 29 */	0x57 0x5f 0x66 0x6d 0x82 0x7e 0x7e 0x7e 0x7d 0x7d 0x7d 0x7d 0x7c 0x7c 0x7c 0x7c 
    .byte $57
    .byte $5f
    .byte $66
    .byte $6d
    .byte $82
    .byte $7e
    .byte $7e
    .byte $7e
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7d
    .byte $7c
    .byte $7c
    .byte $7c
    .byte $7c
//  }




//  data songpatb {
// section songpatb
.pc = * "align"
 .align 256
.pc = * "songpatb"
songpatb:
//  	/* pattern  0 */	0x83 0x00 0x00 0x00 0x88 0x01 0x01 0x01 0x83 0x00 0x00 0x00 0x88 0x01 0x01 0x01 
    .byte $83
    .byte $00
    .byte $00
    .byte $00
    .byte $88
    .byte $01
    .byte $01
    .byte $01
    .byte $83
    .byte $00
    .byte $00
    .byte $00
    .byte $88
    .byte $01
    .byte $01
    .byte $01
//  	/* pattern  1 */	0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
//  	/* pattern  2 */	0x02 0x02 0x03 0x03 0x04 0x04 0x02 0x02 0x03 0x03 0x04 0x04 0x02 0x02 0x03 0x03 
    .byte $02
    .byte $02
    .byte $03
    .byte $03
    .byte $04
    .byte $04
    .byte $02
    .byte $02
    .byte $03
    .byte $03
    .byte $04
    .byte $04
    .byte $02
    .byte $02
    .byte $03
    .byte $03
//  	/* pattern  3 */	0x05 0x05 0x06 0x06 0x07 0x07 0x05 0x05 0x06 0x06 0x07 0x07 0x05 0x05 0x06 0x06 
    .byte $05
    .byte $05
    .byte $06
    .byte $06
    .byte $07
    .byte $07
    .byte $05
    .byte $05
    .byte $06
    .byte $06
    .byte $07
    .byte $07
    .byte $05
    .byte $05
    .byte $06
    .byte $06
//  	/* pattern  4 */	0x08 0x08 0x09 0x09 0x03 0x03 0x08 0x08 0x09 0x09 0x03 0x03 0x08 0x08 0x09 0x09 
    .byte $08
    .byte $08
    .byte $09
    .byte $09
    .byte $03
    .byte $03
    .byte $08
    .byte $08
    .byte $09
    .byte $09
    .byte $03
    .byte $03
    .byte $08
    .byte $08
    .byte $09
    .byte $09
//  	/* pattern  5 */	0x05 0x05 0x09 0x09 0x03 0x03 0x05 0x05 0x09 0x09 0x03 0x03 0x05 0x05 0x09 0x09 
    .byte $05
    .byte $05
    .byte $09
    .byte $09
    .byte $03
    .byte $03
    .byte $05
    .byte $05
    .byte $09
    .byte $09
    .byte $03
    .byte $03
    .byte $05
    .byte $05
    .byte $09
    .byte $09
//  	/* pattern  6 */	0x0a 0x0a 0x02 0x02 0x06 0x06 0x0a 0x0a 0x02 0x02 0x06 0x06 0x0a 0x0a 0x02 0x02 
    .byte $0a
    .byte $0a
    .byte $02
    .byte $02
    .byte $06
    .byte $06
    .byte $0a
    .byte $0a
    .byte $02
    .byte $02
    .byte $06
    .byte $06
    .byte $0a
    .byte $0a
    .byte $02
    .byte $02
//  	/* pattern  7 */	0x80 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 
    .byte $80
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
//  	/* pattern  8 */	0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
//  	/* pattern  9 */	0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x0b 0x04 0x81 0x04 0x81 0x04 0x81 
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $0b
    .byte $04
    .byte $81
    .byte $04
    .byte $81
    .byte $04
    .byte $81
//  	/* pattern 10 */	0x04 0x04 0x81 0x0c 0x04 0x04 0x81 0x0c 0x04 0x81 0x04 0x04 0x04 0x81 0x07 0x07 
    .byte $04
    .byte $04
    .byte $81
    .byte $0c
    .byte $04
    .byte $04
    .byte $81
    .byte $0c
    .byte $04
    .byte $81
    .byte $04
    .byte $04
    .byte $04
    .byte $81
    .byte $07
    .byte $07
//  	/* pattern 11 */	0x81 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x07 0x07 0x81 0x0c 0x07 0x81 0x03 0x03 
    .byte $81
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $07
    .byte $07
    .byte $81
    .byte $0c
    .byte $07
    .byte $81
    .byte $03
    .byte $03
//  	/* pattern 12 */	0x81 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 
    .byte $81
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
//  	/* pattern 13 */	0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x04 0x81 0x04 0x81 0x04 0x81 
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $04
    .byte $81
    .byte $04
    .byte $81
    .byte $04
    .byte $81
//  	/* pattern 14 */	0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
//  	/* pattern 15 */	0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x03 0x81 0x0c 0x0c 0x0d 0x0d 0x04 0x04 0x0d 0x0d 
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $03
    .byte $81
    .byte $0c
    .byte $0c
    .byte $0d
    .byte $0d
    .byte $04
    .byte $04
    .byte $0d
    .byte $0d
//  	/* pattern 16 */	0x81 0x0c 0x0c 0x0c 0x81 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 
    .byte $81
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $81
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
//  	/* pattern 17 */	0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x07 0x81 0x07 0x81 0x07 0x07 0x81 0x0c 0x04 0x04 
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $07
    .byte $81
    .byte $07
    .byte $81
    .byte $07
    .byte $07
    .byte $81
    .byte $0c
    .byte $04
    .byte $04
//  	/* pattern 18 */	0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x03 0x81 0x0c 0x81 0x0d 0x0d 0x04 0x04 0x0d 0x0d 
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $03
    .byte $81
    .byte $0c
    .byte $81
    .byte $0d
    .byte $0d
    .byte $04
    .byte $04
    .byte $0d
    .byte $0d
//  	/* pattern 19 */	0x81 0x0c 0x07 0x07 0x81 0x0c 0x06 0x06 0x81 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 0x0c 
    .byte $81
    .byte $0c
    .byte $07
    .byte $07
    .byte $81
    .byte $0c
    .byte $06
    .byte $06
    .byte $81
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
//  	/* pattern 20 */	0x0c 0x0c 0x0c 0x0c 0x06 0x81 0x06 0x81 0x06 0x81 0x07 0x07 0x81 0x0c 0x04 0x04 
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $0c
    .byte $06
    .byte $81
    .byte $06
    .byte $81
    .byte $06
    .byte $81
    .byte $07
    .byte $07
    .byte $81
    .byte $0c
    .byte $04
    .byte $04
//  	/* pattern 21 */	0x0c 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 0x81 
    .byte $0c
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
    .byte $81
//  	/* pattern 22 */	0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0e 0x0f 0x0f 
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0e
    .byte $0f
    .byte $0f
//  	/* pattern 23 */	0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x0f 0x10 0x10 
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $10
    .byte $10
//  	/* pattern 24 */	0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x10 0x11 0x11 
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $10
    .byte $11
    .byte $11
//  	/* pattern 25 */	0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 0x11 
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
    .byte $11
//  	/* pattern 26 */	0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x06 0x06 
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $06
    .byte $06
//  	/* pattern 27 */	0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x06 0x02 0x02 
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $02
    .byte $02
//  	/* pattern 28 */	0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x02 0x09 0x09 
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $02
    .byte $09
    .byte $09
//  	/* pattern 29 */	0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 0x09 
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
    .byte $09
//  }
//  data songscalefreq {
// section songscalefreq
.pc = * "align"
	.align 256
.pc = * "songscalefreq"
songscalefreq:
//  	10 31 21  7 13 11  8  6 27  9 28 10 10  5 15 17 
    .byte $0a
    .byte $1f
    .byte $15
    .byte $07
    .byte $0d
    .byte $0b
    .byte $08
    .byte $06
    .byte $1b
    .byte $09
    .byte $1c
    .byte $0a
    .byte $0a
    .byte $05
    .byte $0f
    .byte $11
//  	19 23 
    .byte $13
    .byte $17
//  }

// section songseq0
.pc = * "songseq0"
songseq0:
    .byte $00
//  	0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x10 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $10
//  }


//  data songseq1 {
// section songseq1
.pc = * "songseq1"
songseq1:
    .byte $20
//  	0x20 0x30 0x40 0x50 0x20 0x30 0x40 0x50 0x20 0x30 0x40 0x50 0x20 0x30 0x40 0x50 0x20 0x30 0x40 0x50 0x20 0x30 0x40 0x50 0x40 0x30 0x60 0x40 0x40 0x30 0x60 0x40 0x40 0x30 0x60 0x40 0x40 0x30 0x60 0x40 0x10 
    .byte $30
    .byte $40
    .byte $50
    .byte $20
    .byte $30
    .byte $40
    .byte $50
    .byte $20
    .byte $30
    .byte $40
    .byte $50
    .byte $20
    .byte $30
    .byte $40
    .byte $50
    .byte $20
    .byte $30
    .byte $40
    .byte $50
    .byte $20
    .byte $30
    .byte $40
    .byte $50
    .byte $40
    .byte $30
    .byte $60
    .byte $40
    .byte $40
    .byte $30
    .byte $60
    .byte $40
    .byte $40
    .byte $30
    .byte $60
    .byte $40
    .byte $40
    .byte $30
    .byte $60
    .byte $40
    .byte $10
//  }
//  data songseq2 {
// section songseq2
.pc = * "songseq2"
songseq2:
    .byte $70
//  	0x70 0x80 0x80 0x80 0x80 0x80 0x80 0x90 0xa0 0xb0 0xc0 0xd0 0xa0 0xb0 0xc0 0xd0 0xa0 0xb0 0xc0 0xd0 0xa0 0xb0 0xc0 0xe0 0xf0 0x01 0x11 0xc0 0x21 0x31 0x41 0xc0 0xf0 0x01 0x11 0xc0 0x21 0x31 0x41 0xc0 0x51 
    .byte $80
    .byte $80
    .byte $80
    .byte $80
    .byte $80
    .byte $80
    .byte $90
    .byte $a0
    .byte $b0
    .byte $c0
    .byte $d0
    .byte $a0
    .byte $b0
    .byte $c0
    .byte $d0
    .byte $a0
    .byte $b0
    .byte $c0
    .byte $d0
    .byte $a0
    .byte $b0
    .byte $c0
    .byte $e0
    .byte $f0
    .byte $01
    .byte $11
    .byte $c0
    .byte $21
    .byte $31
    .byte $41
    .byte $c0
    .byte $f0
    .byte $01
    .byte $11
    .byte $c0
    .byte $21
    .byte $31
    .byte $41
    .byte $c0
    .byte $51
//  }
//  data songseq3 {
// section songseq3
.pc = * "songseq3"
songseq3:
    .byte $61
//  	0x61 0x71 0x81 0x91 0x61 0x71 0x81 0x91 0x61 0x71 0x81 0x91 0x61 0x71 0x81 0x91 0x61 0x71 0x81 0x91 0x61 0x71 0x81 0x91 0xa1 0xb1 0xc1 0xd1 0xa1 0xb1 0xc1 0xd1 0xa1 0xb1 0xc1 0xd1 0xa1 0xb1 0xc1 0xd1 0x10 
    .byte $71
    .byte $81
    .byte $91
    .byte $61
    .byte $71
    .byte $81
    .byte $91
    .byte $61
    .byte $71
    .byte $81
    .byte $91
    .byte $61
    .byte $71
    .byte $81
    .byte $91
    .byte $61
    .byte $71
    .byte $81
    .byte $91
    .byte $61
    .byte $71
    .byte $81
    .byte $91
    .byte $a1
    .byte $b1
    .byte $c1
    .byte $d1
    .byte $a1
    .byte $b1
    .byte $c1
    .byte $d1
    .byte $a1
    .byte $b1
    .byte $c1
    .byte $d1
    .byte $a1
    .byte $b1
    .byte $c1
    .byte $d1
    .byte $10
//  }




//  data songscalewave {
// section songscalewave
.pc = * "align"
 .align 256
.pc = * "songscalewave"
songscalewave:
//  	 3  8  1  7  1  7  7  7  1  7  1  0  1  7  7  7 
    .byte $03
    .byte $08
    .byte $01
    .byte $07
    .byte $01
    .byte $07
    .byte $07
    .byte $07
    .byte $01
    .byte $07
    .byte $01
    .byte $00
    .byte $01
    .byte $07
    .byte $07
    .byte $07
//  	 7  7 
    .byte $07
    .byte $07
//  }
//  data songenvvol {
// section songenvvol
.pc = * "align"
 .align 256
.pc = * "songenvvol"
songenvvol:
//  	15 15 15 14 12  0  0  0  0  0  0  0  0 15 15 15 
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0e
    .byte $0c
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $0f
    .byte $0f
    .byte $0f
//  	15 14 13 12 11  0  0  0  0  0  0  0  0 11 10  9 
    .byte $0f
    .byte $0e
    .byte $0d
    .byte $0c
    .byte $0b
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $0b
    .byte $0a
    .byte $09
//  	 9  8  7  7  7  6  6  6  6  6  7  7 15  0 14  0 
    .byte $09
    .byte $08
    .byte $07
    .byte $07
    .byte $07
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $06
    .byte $07
    .byte $07
    .byte $0f
    .byte $00
    .byte $0e
    .byte $00
//  	12  0 12  0 12  0 12  0 15 15 14 13 12 11 12 13 
    .byte $0c
    .byte $00
    .byte $0c
    .byte $00
    .byte $0c
    .byte $00
    .byte $0c
    .byte $00
    .byte $0f
    .byte $0f
    .byte $0e
    .byte $0d
    .byte $0c
    .byte $0b
    .byte $0c
    .byte $0d
//  	12 10 12 13 12 10 12 13  6  6 11 11 15 15 15 15 
    .byte $0c
    .byte $0a
    .byte $0c
    .byte $0d
    .byte $0c
    .byte $0a
    .byte $0c
    .byte $0d
    .byte $06
    .byte $06
    .byte $0b
    .byte $0b
    .byte $0f
    .byte $0f
    .byte $0f
    .byte $0f
//  	11 11 13 13  9  9 12 12  6  6 11 11  6  6 11 11 
    .byte $0b
    .byte $0b
    .byte $0d
    .byte $0d
    .byte $09
    .byte $09
    .byte $0c
    .byte $0c
    .byte $06
    .byte $06
    .byte $0b
    .byte $0b
    .byte $06
    .byte $06
    .byte $0b
    .byte $0b
//  	 6  6 10 10  7  7  9  6  6  8  8  5  5  7  4  4 
    .byte $06
    .byte $06
    .byte $0a
    .byte $0a
    .byte $07
    .byte $07
    .byte $09
    .byte $06
    .byte $06
    .byte $08
    .byte $08
    .byte $05
    .byte $05
    .byte $07
    .byte $04
    .byte $04
//  	 6  6  5  5  3  4  4  2  2  4  4  3  4  4  3  3 
    .byte $06
    .byte $06
    .byte $05
    .byte $05
    .byte $03
    .byte $04
    .byte $04
    .byte $02
    .byte $02
    .byte $04
    .byte $04
    .byte $03
    .byte $04
    .byte $04
    .byte $03
    .byte $03
//  	 4  4  3  3  4  4  2  2  4  4 
    .byte $04
    .byte $04
    .byte $03
    .byte $03
    .byte $04
    .byte $04
    .byte $02
    .byte $02
    .byte $04
    .byte $04
//  }
//  data songenvfreq {
// section songenvfreq
.pc = * "align"
	.align 256
.pc = * "songenvfreq"
songenvfreq:
//  	 3 10 26 14 12 10 10 10  0  0  0  0  0  5  6  7 
    .byte $03
    .byte $0a
    .byte $1a
    .byte $0e
    .byte $0c
    .byte $0a
    .byte $0a
    .byte $0a
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $05
    .byte $06
    .byte $07
//  	 8  9 11 12 16  3 10 26 14 12 10 10 10  0  0  0 
    .byte $08
    .byte $09
    .byte $0b
    .byte $0c
    .byte $10
    .byte $03
    .byte $0a
    .byte $1a
    .byte $0e
    .byte $0c
    .byte $0a
    .byte $0a
    .byte $0a
    .byte $00
    .byte $00
    .byte $00
//  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
//  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
//  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
//  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
//  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
//  	 0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
//  	 0  0  0  0  0  0  0  0  0  0 
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
    .byte $00
//  }
//  inline song_seq_wrap { x?41 >={ x=0 } }
// section tickcount
.pc = * "player"
tickcount:
    .byte $08
//  	song_step_ticks_0
//  	song_step_ticks_1
//  	song_step_ticks_2
    .byte $08
//  	song_step_ticks_3
    .byte $08
//  }
    .byte $07
//  
// section song_player
song_player:
    ldx     songpos_seq
//  	x=songpos_seq
//  	a=songseq0,x
    lda     songseq0,x
//  	player_prep_pointers
//  	player_fetch_volume
	// inlined player_prep_pointers
    tay
    and     #$f0
    sta     ptra
    tya
    and     #$0f
    clc
    adc     #>(songpata)
    sta     ptra+1
    ldy     songpos_step
    lda     (ptra),y
    sta     ptrb
	// end of inlined player_prep_pointers
//  	!={
	// inlined player_fetch_volume
    ldy     songpos_tick
    lda     #>(songenvvol)
    sta     ptrb+1
    lda     (ptrb),y
	// end of inlined player_fetch_volume
    beq     __label_6
//  		av0=a
    sta     av0
//  	} else {
    jmp     __label_7
__label_6:
//  		// channel 1 play through
    ldx     songpos_seq
//  		x=songpos_seq
//  		a=songseq1,x
    lda     songseq1,x
//  		player_prep_pointers
//  		player_fetch_volume		av0=a
	// inlined player_prep_pointers
    tay
    and     #$f0
    sta     ptra
    tya
    and     #$0f
    clc
    adc     #>(songpata)
    sta     ptra+1
    ldy     songpos_step
    lda     (ptra),y
    sta     ptrb
	// end of inlined player_prep_pointers
	// inlined player_fetch_volume
    ldy     songpos_tick
    lda     #>(songenvvol)
    sta     ptrb+1
    lda     (ptrb),y
	// end of inlined player_fetch_volume
    sta     av0
//  	}
//  	player_fetch_freq_wave  af0=a ac0=x
__label_7:
	// inlined player_fetch_freq_wave
    lda     ptra+1
    sec
    sbc     #>(songpata)
    clc
    adc     #>(songpatb)
    sta     ptra+1
    ldy     songpos_step
    lda     (ptra),y
    tax
	bpl	__i14__label_4
    ldy     songpos_tick
    lda     #>(songenvfreq)
    sta     ptrb+1
    lda     (ptrb),y
	jmp	__i14__label_5
__i14__label_4:
    lda     songscalefreq,x
    tay
    lda     songscalewave,x
    tax
    tya
__i14__label_5:
	// end of inlined player_fetch_freq_wave
    sta     af0
    stx     ac0
//  
//  	x=songpos_seq
    ldx     songpos_seq
//  	a=songseq2,x
    lda     songseq2,x
//  	player_prep_pointers
//  	player_fetch_volume
	// inlined player_prep_pointers
    tay
    and     #$f0
    sta     ptra
    tya
    and     #$0f
    clc
    adc     #>(songpata)
    sta     ptra+1
    ldy     songpos_step
    lda     (ptra),y
    sta     ptrb
	// end of inlined player_prep_pointers
//  	!={
	// inlined player_fetch_volume
    ldy     songpos_tick
    lda     #>(songenvvol)
    sta     ptrb+1
    lda     (ptrb),y
	// end of inlined player_fetch_volume
    beq     __label_8
//  		av1=a
    sta     av1
//  	} else {
    jmp     __label_9
__label_8:
//  		// channel 3 play through
    ldx     songpos_seq
//  		x=songpos_seq
//  		a=songseq3,x
    lda     songseq3,x
//  		player_prep_pointers
//  		player_fetch_volume		av1=a
	// inlined player_prep_pointers
    tay
    and     #$f0
    sta     ptra
    tya
    and     #$0f
    clc
    adc     #>(songpata)
    sta     ptra+1
    ldy     songpos_step
    lda     (ptra),y
    sta     ptrb
	// end of inlined player_prep_pointers
	// inlined player_fetch_volume
    ldy     songpos_tick
    lda     #>(songenvvol)
    sta     ptrb+1
    lda     (ptrb),y
	// end of inlined player_fetch_volume
    sta     av1
//  	}
//  	player_fetch_freq_wave	af1=a ac1=x
__label_9:
	// inlined player_fetch_freq_wave
    lda     ptra+1
    sec
    sbc     #>(songpata)
    clc
    adc     #>(songpatb)
    sta     ptra+1
    ldy     songpos_step
    lda     (ptra),y
    tax
	bpl	__i19__label_4
    ldy     songpos_tick
    lda     #>(songenvfreq)
    sta     ptrb+1
    lda     (ptrb),y
	jmp	__i19__label_5
__i19__label_4:
    lda     songscalefreq,x
    tay
    lda     songscalewave,x
    tax
    tya
__i19__label_5:
	// end of inlined player_fetch_freq_wave
    sta     af1
    stx     ac1
//  
//  	// next tick/step/pattern
//  	x=songpos_tick x++
    ldx     songpos_tick
    inx
//  	a=songpos_step a&3 y=a a=tickcount,y ptra=a
    lda     songpos_step
    and     #$03
    tay
    lda     tickcount,y
    sta     ptra
//  	x?ptra
    cpx     ptra
//  
//  
//  	//x?6
//  	 !={ songpos_tick=x }
    beq     __label_10
    stx     songpos_tick
//  	else {
    jmp     __label_11
__label_10:
//  		songpos_tick=x=0
    ldx     #$00
    stx     songpos_tick
//  		a=songpos_step c- a+1 song_step_wrap songpos_step=a
    lda     songpos_step
    clc
    adc     #$01
	// inlined song_step_wrap
    and     #$0f
	// end of inlined song_step_wrap
    sta     songpos_step
//  		=={
    bne     __label_12
//  			x=songpos_seq x++ song_seq_wrap songpos_seq=x
    ldx     songpos_seq
    inx
	// inlined song_seq_wrap
    cpx     #$28
	bcc	__i21__label_1
    ldx     #$00
__i21__label_1:
	// end of inlined song_seq_wrap
    stx     songpos_seq
//  		}
//  	}
__label_12:
//  }
__label_11:
//  
    rts
// section main
